# Internet, une caverne d'Ali baba

Internet est un gigantesque réservoir de services et de savoirs. Voici une
liste évidemment non exhaustive de liens vers des outils qui peuvent vous être
utiles en tant que DAFS.

## Les CMS

Un Content Management System permet de faire des sites facilement modifiables par une
interface web.

* [Spip](http://www.spip.net/fr)
* [Joomla](https://www.joomla.fr/)
* [Drupal](https://www.drupal.org/)
* [Wordpress](https://fr.wordpress.org/)
* [Mezzanine](http://mezzanine.jupo.org/)
* [Wagtail](https://wagtail.io/)
* [Plone](https://plone.org/)

## Les moteurs de wiki

Les wikis popularisés par la très célèbre Wikipedia ont de multiples utilités
: coopération, documentation, partage d'informations, ...

* [DokuWiki](https://www.dokuwiki.org/)
* [MediaWiki](https://www.mediawiki.org/)
* [Un comparatif](http://www.wikimatrix.org)

## Les générateurs de sites statiques

Il permettent de générer des sites simples et statiques à partir de fichiers
textes au format markdown ou ReST. Très utile pour de la documentation par
exemple.

* [Pelican](http://blog.getpelican.com/)
* [Jekyll](http://jekyllrb.com/)
* [Sphinx](http://www.sphinx-doc.org)


## Les services pour la création d’IHM

Des outils pour les créations graphiques de vos sites web. Ces outils seront
abordés lors du cours de maquette graphique.

* [Pencil](https://github.com/prikhi/pencil)
* [Mockup](https://moqups.com/)
* [balsamiq](https://balsamiq.com/index.html)
* [Wireframe](https://wireframe.cc/)
* [Cacoo](https://cacoo.com/lang/fr/)
* [Iplotz](http://iplotz.com/)
* [Mockflow](https://mockflow.com/)
* [Gomockingbird](https://gomockingbird.com/home)

## Les services de gestion de projet agile

Servent de support à l'application de méthode agiles.

* [Taiga](https://taiga.io/)
* [Trello](https://trello.com/)
* [Framaboard](https://framaboard.org/)
* [Propulse](http://www.ppulse.fr/)
* [Kanboard](https://kanboard.net/)
* [Tuleap](https://www.tuleap.org/)

## Les plate-formes d’hébergement de code

La très célèbre Github est incontournable, mais il en existe d'autres.

* [Github](https://github.com/)
* [Gitlab](https://about.gitlab.com/)
* [Bitbucket](https://bitbucket.org/)

## Démonstrateur en ligne

* [Js fidle](http://jsfiddle.net/)
* [Python fiddle](http://pythonfiddle.com/)
* [Codepen](http://codepen.io/)
* [Plunker](https://plnkr.co/)

## Les outils de veille

La veille sera abordée dans la suite du cours.

* [Framanews](https://framanews.org/)
* [Netvibes](http://www.netvibes.com/)
* [Feedly](http://www.feedly.com/)
* [Leed](http://leed.idleman.fr/)
* [TtRSS](https://www.tt-rss.org/gitlab/fox/tt-rss)
* [Selfoss](http://selfoss.aditu.de/)

## Les moocs et tutoriels

Continuez à progresser avec les MOOCs.

* [France université numérique](https://www.fun-mooc.fr/)
* [Openclassroom](https://openclassrooms.com/)
* [Khan academy](http://www.khanacademy.org/)
* [Checkio](https://checkio.org/)

## Les services de partage et stokage de fichiers

Permet de partager rapidement un fichier avec un client par exemple.

* [Framadrop](https://framadrop.org/)
* [Dl free](http://dl.free.fr)
* [Sharedrop](https://www.sharedrop.io)
* [Dropbox](https://www.dropbox.com/)
* [WeTransfer](https://www.wetransfer.com/)

Attention : Posez vous la question de ce qui est fait avec vos données...

## Hébergeurs et/ou registrar

Permettent d'héberger vos sites ou acquérir les noms de domaines associés

* [Ovh](https://www.ovh.com/fr/)
* [1and1](http://www.1and1.fr/)
* [Ghandi](https://www.gandi.net/)
* [Bookmyname](https://www.bookmyname.com/)

## Réseaux sociaux

Devenus incontournables, vous pouvez les utiliser à bon escient dans votre
pratique.

* [Twitter](https://twitter.com/)
* [Stackoverflow](http://stackoverflow.com/)
* [Facebook](https://www.facebook.com/)
* [Diaspora](https://joindiaspora.com/)
* [Codingame](https://www.codingame.com)
* [Linkedin](https://www.linkedin.com/)

## Conférence Audio/video

Parfois utile quand on veut contacter une personne éloignée ou faire des
démonstrations à distance.

* [Skype](https://www.skype.com/fr/)
* [Jitsi](https://jitsi.org/)
* [Appears.in](https://appear.in/)
* [Linphone](http://www.linphone.org/)



